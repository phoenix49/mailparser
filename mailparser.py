#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  untitled.py
#  
#  Copyright 2013 Said Babayev <firefly@said-ubuntu>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# This is a webapp to search and display data from MongoDB using Tornado app framework

import tornado.ioloop
import tornado.web
from parser import DBConnection

global serverurl
global version
serverurl = 'http://billinghouse.azercell.com/'
version = '1.6'

class BaseHandler(tornado.web.RequestHandler):
    def getArgs(self):
        self.address = ''
        if len(self.request.arguments) > 0:
            #extract values from lists
            if len(self.get_arguments('address')) > 0:
                self.address = self.get_arguments('address')[0]
    
    def getData(self):
        db = DBConnection()
        self.data = db.search(self.address)
        db.disconnect()
        del db

class WorkHandler(BaseHandler):
    def get(self):
        self.redirect("/")

    def post(self):
        self.getArgs()
        if len(self.address) > 0:
            self.getData()
            self.render("table.html", data=self.data, version=version)
        else:
            self.get()

class StartHandler(BaseHandler):
    def get(self):
        self.write('<html><head><title>Billing House mail parser for Azercell</title></head>'
                   '<body><form action="/work" method="post">'
                   '<div align="center">'
                   '<h4>Billing House mail parser for Azercell</h4>'
                   'Full or part of an e-mail address: <input type="text" name="address">'
                   '<input type="submit" value="Search">'
                   '<h6>version {0}</h6>'
                   '</div>'
                   '</form></body></html>'.format(version))

if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/", StartHandler),
        (r"/work", WorkHandler),
        ])
    application.listen(8080, '127.0.0.1')
    tornado.ioloop.IOLoop.instance().start()
