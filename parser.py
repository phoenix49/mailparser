#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parser.py
#  
#  Copyright 2013 Said Babayev <firefly@said-ubuntu>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# This program will parse postfix log messages and prepare them for MongoDB
# Data structure in MongoDB:
#   { <id1> : { _id: ..., from: ..., to: ....}, <id2> : { _id: ..., from: ..., to: ....} }

import os
import re
import pymongo

class DBConnection(object):
    """ Class updates MongoDB database with postfix data """
    def __init__(self):
        self.connection = pymongo.Connection()
        
    def disconnect(self):
        self.connection.disconnect()

    def insert(self, data):
        #Create collection for each day
        db = self.connection['bdb']
        for key, values in data.items():
            collection = db[key.replace(' ', '_')]
            for v in values.values():
                id = collection.save(v)
    
    def readdata(self):
        #Read all available data and print to stdout
        db = self.connection['bdb']
        for day in db.collection_names():
            col = db[day]
            for record in col.find():
                print 'Record:', record
    
    def search(self, address):
        #Search for an address
        ids = []
        db = self.connection['bdb']
        for day in db.collection_names():
            col = db[day]
            ans = col.find({'to' : {'$regex' : '.*{0}.*'.format(address)}})
            for k in ans:
                ids.append(k)
        return ids

class Parser(object):
    """
    This class parses postfix log messages and prepare them for MongoDB
    Data structure for insertion:
    { <date1> : { <id1> : { _id: ..., from: ..., to: ....}, <id2> : { _id: ..., from: ..., to: ....} }, <date2> : ... }
    Each key (date) should be out in separate collection in mongodb
    """
    def __init__(self):
        #Log location
        self.logloc = '/var/log/'
        #Log prefix
        self.logprefix = 'maillog'
        #'date' here is a key for Mongo queries
        self.data = {}
        #Regexes
        self.queueid = re.compile(r': ([0-9A-Z]{7,8}): ')
        self.toaddr = re.compile(r': to=<(.+@.+\.\w{2,3}\.?\w*?)>, ')
        self.relay = re.compile(r'relay=(.*?), ')
        # 1 = status, 2 = return messsage from server
        self.status = re.compile(r'status=(.*?) (.*$)')
    
    def populate(self):
        #Read log files into memory
        #find maillog files
        mlogs = []
        for mlog in os.listdir(self.logloc):
            if mlog.startswith(self.logprefix):
                mlogs.append(os.path.join(self.logloc, mlog))
        f = ( open(x) for x in mlogs )
        #Read all data into temp var
        logfiles = ( y.read().split('\n') for y in f )
        #Populate into self.data
        for rawline in logfiles:
            #Each rawline from each file is list of records
            for line in rawline:
                #Split each line into comma separated line
                line = line.split()
                if 'from=<billing_house@azercell.com>,' in line:
                    date = line[0] + ' ' + line[1]
                    id = line[5].strip(':')
                    if date in self.data:
                        self.data[date].update({id : { '_id' : id, 'from' : 'billing_house@azercell.com' } })
                    else:
                        self.data[date] = {id : { '_id': id, 'from' : 'billing_house@azercell.com' } }
            #Extract and fill other fields
            for line in rawline:
                line = line.split()
                resid = self.queueid.search(' '.join(line))
                resto = self.toaddr.search(' '.join(line))
                if resid and resto:
                    date = line[0] + ' ' + line[1]
                    try:
                        self.data[date][resid.group(1)].update({'to':resto.group(1), 'status':self.status.search(' '.join(line)).group(1), \
                            'result': self.status.search(' '.join(line)).group(2), 'date': date})
                    except:
                        #If no key - then sender is not billing_house
                        pass
        del logfiles
        return self.data

def main():
    p = Parser()
    data = p.populate()
    db = DBConnection()
    db.insert(data)
    #db.readdata()
    #db.search('gmail.com')
    return 0

if __name__ == '__main__':
    main()

