MailParser
======================

Requirements:
 * MongoDB
 * PyMongo
 * Python 2.6+
 * Python Tornado web engine
 * Postfix compatible logfiles

Usage:
 * Add parser.py to cron, and update your database on regular basis
 * Start mailparser.py using nohup
 * Configure fronend server and proxy requests to 127.0.0.1:8080
